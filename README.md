# macos-gitlabrunner

GitLab Runner on macOS

---

GitLab runner can be run on macOS! However, it is recommend that you use an executor type that allows for virutalization/clean built environment on each run.

There are two technology types that GitLab runner already works with, `Paralells` and `VirtualBox`.

## Getting macOS

The easist way (thanks to online help) is to download the macOS you need via the App store and create an ISO image from the downloaded install application.

For example, macOS Big Sur is available at the time of this writing. Go to the Mac App Store, and search for Big Sir, and download it. It will place an Installer App in your `/Applications` directory.

### Creating an ISO

After you have downloaded the installer, it is time to create an ISO.

Run the following script (you have to have to change some values at the top of the script if you're using a different OS than Big Sur)

> One command has to be run as root. You must be an administrator and type in your password.

```bash
images/iso/base/scripts/create_iso.sh
```

You will get an iso named `macOSImage.iso` (or according to the name you've specified). You can use this ISO to create a base image both in VirtualBox and Parallels!


## Virtualbox Base Image

After the creation of the ISO, we can use it to create a macOS VirtualBox image.

### Parallels Base Image

You will need to [download and install](https://www.parallels.com/products/desktop/download/) Parallels Desktop and the Parallels SDK onto the *__host__* machine.

The first thing to do is to create a macOS image from the Recovery Partition in Parallels.

- Open Parallels
    - Open Control Center if you have existing VMs, and click on the `+` to add a new VM.
- Under `Free Systems` scroll to 


