#!/usr/bin/env bash

# Credit for base commands/instructions goes to the following:
# - https://osxdaily.com/2020/07/20/how-convert-macos-installer-iso/
# - https://blog.petehouston.com/download-and-convert-macos-mojave-installer-into-iso-file/
# - https://www.reddit.com/r/hackintosh/comments/a9twye/mac_os_1014_mojave_vmware_15_diy_guide_no_3rd/
# - http://hints.macworld.com/article.php?story=20020426083026328
# - https://support.apple.com/en-us/HT201372


set -e

NAME="macOSImage"
DMG_FILE_PATH="/tmp/${NAME}.dmg"
MOUNT_PATH="/Volumes/${NAME}"
INSTALL_APP_NAME="Install macOS Big Sur"

if [[ -f ${DMG_FILE_PATH} ]]
then
    rm ${DMG_FILE_PATH}
fi

hdiutil create -o ${DMG_FILE_PATH} -size 15360m -volname ${NAME} -layout SPUD -fs HFS+J
hdiutil attach ${DMG_FILE_PATH} -noverify -mountpoint ${MOUNT_PATH}
sudo "/Applications/${INSTALL_APP_NAME}.app/Contents/Resources/createinstallmedia" --volume ${MOUNT_PATH} --nointeraction
hdiutil detach -force "/Volumes/${INSTALL_APP_NAME}"
hdiutil convert ${DMG_FILE_PATH} -format UDTO -o ~/${NAME}.cdr
mv ~/${NAME}.cdr ~/${NAME}.iso
rm ${DMG_FILE_PATH}
