#!/usr/bin/env bash
set -e

# Credit for base instructions/commands goes to:
# - https://www.andreafortuna.org/2019/10/24/how-to-create-a-virtualbox-vm-from-command-line/
# - https://www.techrepublic.com/article/how-to-run-virtualbox-virtual-machines-from-the-command-line
# - VirtualBox Documenation


ISO_PATH="${HOME}/macOSImage.iso"
VM_NAME="macOS GitLab Runner Base"

VMS_LIST=$(VBoxManage list vms)
set +e
echo "${VMS_LIST}" | grep "${VM_NAME}" > /dev/null
STATUS_CODE=${?}
set -e
if [[ ${STATUS_CODE} -eq 0 ]]
then
    read -p "VM with the same name (${VM_NAME}) already exists. Can I delete/purge all files? (y/N) ==> " DELETE_CONFIRM_ANSWER
    if [[ "${DELETE_CONFIRM_ANSWER}" == "y" ]] || [[ "${DELETE_CONFIRM_ANSWER}" == "Y" ]]
    then
        VBoxManage unregistervm "${VM_NAME}" --delete
    else
        echo "[ERROR] Could not complete script, VM name collision."
        exit 1
    fi
fi

CREATE_VM_OUTPUT=$(VBoxManage createvm --name "${VM_NAME}" --ostype "MacOS_64" --register)
echo "Created VM"
VM_CONFIG_FULL_PATH=$(echo "${CREATE_VM_OUTPUT}" | tail -1 | awk '{for (i=3; i<NF; i++) printf $i " "; print $NF}' | sed "s/'//g")
VM_DIR=$(dirname "${VM_CONFIG_FULL_PATH}")
VM_HD_PATH="${VM_DIR}/${VM_NAME}.vhd"

echo "Modifying VM"
VBoxManage modifyvm "${VM_NAME}" --ioapic on --memory 2048 --vram 21 --nic1 nat --chipset ich9 --firmware efi --mouse usb --keyboard usb --usbohci on
VBoxManage createhd --filename "${VM_HD_PATH}" --size 40960 --format VHD
VBoxManage storagectl "${VM_NAME}" --name "SATA Controller" --add sata --controller IntelAhci
VBoxManage storageattach "${VM_NAME}" --storagectl "SATA Controller" --port 0 --device 0 --type hdd --medium "${VM_HD_PATH}"
VBoxManage storageattach "${VM_NAME}" --storagectl "SATA Controller" --port 1 --device 0 --type dvddrive --medium "${ISO_PATH}"
VBoxManage modifyvm "${VM_NAME}" --boot1 dvd --boot2 disk --boot3 none --boot4 none
echo "Modifications Complete"

echo "Starting VM"
VBoxManage startvm "${VM_NAME}"
echo "Please continue installation and run the snapshot script."
